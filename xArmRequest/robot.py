import xarm


class RobotClient:

    def __init__(self):
        self.robot = self.connect_to_robot()

    def move(self, val1, val2, val3, val4, val5, val6):
        self.robot.setPosition(1, val1, wait=True)
        self.robot.setPosition(2, val2, wait=False)
        self.robot.setPosition(3, val3, wait=False)
        self.robot.setPosition(4, val4, wait=False)
        self.robot.setPosition(5, val5, wait=False)
        self.robot.setPosition(6, val6, wait=True)
        return 'done'

    def connect_to_robot(self):
        try:
            arm = xarm.Controller("USB")
            print("Connected")
            return arm
        except:
            print("Problem connecting to robot. Check connections")
            return None
