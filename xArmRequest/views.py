import json
from numbers import Number
import django
from django.http import HttpRequest, HttpResponse
from xArmRequest.robot import RobotClient
from django.views.decorators.csrf import csrf_exempt

robot = RobotClient()


@csrf_exempt
def move_robot(request: HttpRequest):
    params = request.body.decode('utf-8')
    body = json.loads(params)
    gripper =body['gripper']
    w= body['w']
    x =body['x']
    y= body['y']
    z= body['z']
    base = body['base']
    complete = robot.move(gripper, w, x, y, z, base)
    if complete == 'done':
        return HttpResponse("Hello, world.")
    

